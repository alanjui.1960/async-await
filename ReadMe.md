# Node.js Async/Await 研習

學習 Node.js 新增指令 Async/Await 用法，與提供程式碼編撰時之參考範例。

參考文章： [The 80/20 Guide to Async/Await in Node.js](http://thecodebarbarian.com/80-20-guide-to-async-await-in-node.js.html)

 
### async01-return-values.js  
接收 function 的傳回值。

### async02-exceptions.js     
可用 try/catch 補捉程式「出狀況」的意外，並加以處理。

### async03-try-catch.js  

### async04-for-loops.js  
可在 for 迴圈中使用 await

### async05-if-condictions.js
可在 if 判斷中使用 await  

### async06-not-sequence.js    
須留心 Node.js 程序語言：「非阻斷式程式」的特性，程式碼的執行，往往不是「逐一執行」

### async07-its-parallel.js            
即便是 async function ，也該留心它有「平行執行」的特性

### async08-express-error-handling.js
在 express 中使用 async function 中，若無 expection 處理，將導致系統掉入「等待處理的迴圈」之中，令系統看似當機

### async09-express-error-safe-handler.js
在 express 中使用 async function 時，標準寫法：具有 exception 處理的 .catch()  

