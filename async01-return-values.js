async function test() {
  const res = await new Promise((resolve) => {
    setTimeout(() => resolve('Hello, World!'), 10000);
  });
  console.log(res);
}

test();