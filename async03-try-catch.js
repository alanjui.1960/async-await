function bad() {
  throw new Error('bad');
}

function bad2() {
  return new Promise(() => { throw new Error('bad2'); });
}

async function test() {
  try {
    await bad();
  } catch (error) {
    console.log('caught', error.message);
  }

  try {
    await bad2();
  } catch (error) {
    console.log('caught', error.message);
  }
}

test();