function wait(ms) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), ms);
  });
}

async function test() {
  for ( let i = 0; i < 10; ++i ) {
    if ( i < 5 ) {
      await wait(1000);
    }

    console.log('Hello, World!');
  }
}

test();