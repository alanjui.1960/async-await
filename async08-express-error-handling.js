const express = require('express');
const app = express();

app.get('/', handler);

app.listen(3000);
console.log('Server is listening on port: 3000');

function wait(ms) {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  });
}

async function handler(req, res) {
  await new Promise((resolve, reject) => {
    reject(new Error('Record not found!'));
  });
  res.send('Hello, world!');

  // throw new Error('Hang!!');
}