const express = require('express');
const app = express();

app.get('/', listAll(handler));

app.listen(3000);
console.log('Server is listening on port: 3000');

function listAll(handler) {
  return function (req, res) {
    handler(req, res)
      .catch((error) => res.status(500).send(error.message));
  }
}

async function handler(req, res) {
  await new Promise((resolve, reject) => {
    reject(new Error('Record not found!'));
  });
  res.send('Hello, world!');
}